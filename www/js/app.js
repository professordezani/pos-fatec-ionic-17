
angular.module('starter', ['ionic', 'firebase'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.controller('MainCtrl', function($scope, $firebaseArray) {

    //https://fatectodopos17.firebaseio.com/tarefas
    var ref = firebase.database().ref().child('tarefas');
    $scope.tarefas = $firebaseArray(ref);

    $scope.tarefa = {};

    $scope.salvar = function(t) {

      var tarefa = angular.copy(t);
      tarefa.concluida = false;
      tarefa.data = new Date().getTime();

      $scope.tarefas.$add(tarefa);
    }

})